#!/bin/sh
# clean /pld filesystem, so it could be used as clean vm base
set -xe

if [ -n "$1" ]; then
	root=$1
else
	. ./_config.sh

	root=$INSTALL_ROOT
	boot=$BOOT_DEVICE

	# must be mounted
	mountpoint $root
	test -n "$boot"  && mountpoint -q $root/boot
fi

# another check, root dir must exist
test -d $root

# cmp tool must exist
which cmp

# safety check. no .rpm{new,save,orig} allowed
if [ "$ALLOW_RPMSAVE" != "yes" ]; then
	rpmcf=$(find $root '(' -name '*.rpmnew' -o -name '*.rpmsave' -o -name '*.rpmorig' ')' -print)
	if [ -n "$rpmcf" ]; then
		echo >&2 "rpm configs found, aborting"
		ls -ld $rpmcf
		exit 1
	fi
fi

inline_edit() {
	local pattern=$1 file=$2
	sed -i~ -e "$pattern" $file
	cmp -s $file{,~} && mv -f $file{~,} || :
}

truncate="
$root/var/log/{lastlog,faillog,wtmpx,cron,dmesg}
$root/etc/resolv.conf
"
for file in $truncate; do
	test -f $file || continue
	test -s $file || continue
	echo "Truncate: $file"
	> $file
done

# use google dns if not acquired one from dhcp
echo 'nameserver 8.8.8.8' > $root/etc/resolv.conf

rm -vf $root/root/.{history,history.mksh,cd_history,poldek_history,lesshst}

# remove unwanted poldek config s
test -e $root/etc/poldek/repos.d/delfi.conf && chroot $root poldek -ev --noask poldek-delfi
rm -f $root/etc/poldek/repos.d/carme.conf

# make rpmdb minimal
if [ "$COMPACT_RPMDB" != "no" ]; then
	db5.2_load -r lsn $root/var/lib/rpm/Packages
	test ! -d $root/var/lib/rpmdb.$$
	test ! -d $root/var/lib/rpmbak.$$
	mkdir -p $root/var/lib/rpmdb.$$
	mv $root/var/lib/{rpm,rpmdb.$$}/Packages
	test -f $root/var/lib/rpm/DB_CONFIG && mv $root/var/lib/{rpm,rpmdb.$$}/DB_CONFIG
	mv $root/var/lib/{rpm,rpmbak.$$}
	mv $root/var/lib/{rpmdb.$$,rpm}
	rm -rfv $root/var/lib/rpmbak.$$
	install -p _rpm-firstboot.sh $root/usr/lib/rpm/firstboot
	install -p _rc.firstboot.sh $root/etc/rc.d/rc.firstboot
	grep -q rc.firstboot $root/etc/rc.d/rc.local || \
		echo /etc/rc.d/rc.firstboot >> $root/etc/rc.d/rc.local
fi

# be sure /dev/console exists
mountpoint $root/dev && umount $root/dev
test -c $root/dev/console || mknod -m 660 $root/dev/console c 5 1
test -c $root/dev/null || mknod -m 666 $root/dev/null c 1 3
test -c $root/dev/zero || mknod -m 666 $root/dev/zero c 1 5

# convert to file
# vboxsf mount -a would add duplicates otherwise
if [ -L $root/etc/mtab ]; then
	rm -f $root/etc/mtab
	> $root/etc/mtab
fi

# not wanted
# cleanup state files and history
rm -f $root/root/.{viminfo,mailrc}
rm -rfv $root/root/tmp/*
test -d $root/root/root && rmdir $root/root/root
test -d $root/root/rcd-inst && rmdir $root/root/rcd-inst
rm -rfv $root/var/log/dmesg.*
rm -f $root/var/lib/rpm/__db.*
rm -rfv $root/var/lib/rpm.rpmbackup*
rm -rfv $root/var/lib/wdj
rm -f $root/var/lib/banner/*
rm -f $root/var/run/utmpx
rm -f $root/var/run/runlevel.dir
rm -f $root/var/run/random-seed
rm -f $root/var/run/cron*
rm -f $root/var/run/*.pid
rm -rfv $root/var/run/dhcpcd
rm -f $root/var/cache/rc-scripts/*
rm -f $root/var/cache/ldconfig/*
test "$KEEP_TMP" != "yes" && rm -rfv $root/tmp/*
rm -rfv $root/var/cache/poldek/*
rm -rfv $root/var/cache/hrmib/*
rm -rfvv $root/var/spool/repackage/*
rm -fv $root/etc/ssh/*key*
rm -fv $root/etc/ld.so.cache
rm -fv $root/root/.ssh/known_hosts
rm -rfv $root/tmp/.ICE-unix
rm -f $root/geninitrd.sh
rm -f $root/poweroff
rm -f $root/boot/*.old
rm -rfv $root/etc/lvm/{archive,backup,cache}
rm -rfv $root/run/*

rm -rfv $root/usr/share/man/man?/*
rm -rfv $root/usr/share/man/{??,???,*_*,*@*}
rm -rfv $root/usr/share/locale/*/
rm -rfv $root/usr/share/help/*/
rm -rfv $root/etc/sysconfig/locale/*/
rm -rfv $root/usr/share/info/*
rm -rfv $root/usr/share/doc/*
rm -rfv $root/usr/src/examples/*
rm -rfv $root/usr/share/pixmaps/*
rm -rfv $root/usr/share/bash
rm -rfv $root/etc/skel/{etc,tmp}

# don't need if already installed to /boot
rm -rfv $root/lib/grub/i386-pc

# ipv6 off
if [ -f $root/etc/sysconfig/network ]; then
	grep -q IPV6_NETWORKING=yes $root/etc/sysconfig/network && \
	sed -i -e 's/IPV6_NETWORKING=yes/IPV6_NETWORKING=no/' $root/etc/sysconfig/network
fi

# update poldek sources to use master mirror
inline_edit "s#^\(_prefix\s*\)=.*#\1= %{_pld_main_prefix}#" $root/etc/poldek/repos.d/pld.conf
# use /var/cache/poldek
inline_edit "s#^\#\?\(cachedir\s*\)=.*#\1= /var/cache/poldek#" $root/etc/poldek/poldek.conf

# clean final backups
find "$root" '(' -name "*~" -o -name ".??*~" -o -name '*.old' -o -name '*.rpmsave' ')' -print0 | xargs -0 rm -fv
