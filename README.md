# PLD Linux base docker images

This project builds and hosts PLD Linux (Th) base images

Available images:
- `registry.gitlab.com/pld-linux/pld:latest` - latest `th-x86_64`
- `registry.gitlab.com/pld-linux/pld/i686:latest` - latest `th-i686` build

Also you can retrieve historical date based images:
- `registry.gitlab.com/pld-linux/pld/releases/2018:20180827-x86_64` - tagged with build date
- `registry.gitlab.com/pld-linux/pld/releases/2018:20180827-i686` - tagged with build date

## Snapshot images

There's also builds available from Th snapshot:

| Snapshot |  Arch  | Image URL                                                |
|:--------:|:------:|----------------------------------------------------------|
|   2012   | i686   | `registry.gitlab.com/pld-linux/pld/snapshot:2012-i686`   |
|   2012   | x86_64 | `registry.gitlab.com/pld-linux/pld/snapshot:2012-x86_64` |
|   2013   | i686   | `registry.gitlab.com/pld-linux/pld/snapshot:2013-i686`   |
|   2013   | x86_64 | `registry.gitlab.com/pld-linux/pld/snapshot:2013-x86_64` |
|   2014   | i686   | `registry.gitlab.com/pld-linux/pld/snapshot:2014-i686`   |
|   2014   | x86_64 | `registry.gitlab.com/pld-linux/pld/snapshot:2014-x86_64` |
|   2015   | i686   | `registry.gitlab.com/pld-linux/pld/snapshot:2015-i686`   |
|   2015   | x86_64 | `registry.gitlab.com/pld-linux/pld/snapshot:2015-x86_64` |
|   2016   | i686   | `registry.gitlab.com/pld-linux/pld/snapshot:2016-i686`   |
|   2016   | x86_64 | `registry.gitlab.com/pld-linux/pld/snapshot:2016-x86_64` |
|   2017   | i686   | `registry.gitlab.com/pld-linux/pld/snapshot:2017-i686`   |
|   2017   | x86_64 | `registry.gitlab.com/pld-linux/pld/snapshot:2017-x86_64` |
|   2018   | i686   | `registry.gitlab.com/pld-linux/pld/snapshot:2018-i686`   |
|   2018   | x86_64 | `registry.gitlab.com/pld-linux/pld/snapshot:2018-x86_64` |
|   2019   | i686   | `registry.gitlab.com/pld-linux/pld/snapshot:2019-i686`   |
|   2019   | x86_64 | `registry.gitlab.com/pld-linux/pld/snapshot:2019-x86_64` |

These images are manually built, and they don't change (as snapshot does not change).

Example: builds from `th/2019` snapshot for `x86_64` architecture:
```
./build-snap.sh 2019 x86_64 th
```
