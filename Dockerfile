ARG BOOTSTRAP_IMAGE=registry.gitlab.com/pld-linux/pld/bootstrap:latest

FROM $BOOTSTRAP_IMAGE AS build

ARG dist=th
ARG arch=x86_64

WORKDIR /build
COPY . .

RUN set -x \
	&& SKIP_DOCKER=1 sh -x ./docker.sh /pld "$dist" "$arch"

FROM scratch
COPY --from=build /pld .
